//=============================================================================
// WarVoteDisplay.
//=============================================================================
class WV_Display expands UWindowDialogClientWindow;

var RuneMenuLabelControl MapListLabel;
var RuneMenuLabelControl GameTypeLabel;
var RuneMenuLabelControl ChatLabel;

var RuneMenuLabelControl MOTD1Label;
var RuneMenuLabelControl MOTD2Label;
var RuneMenuLabelControl MOTD3Label;
var RuneMenuLabelControl MOTD4Label;
var RuneMenuLabelControl VoteTimeLabel;
var UWindowSmallButton VoteButton;
var UWindowSmallButton WebsiteButton;
var UWindowSmallButton DiscordButton;
var UWindowComboControl GameTypeSelect;
var UWindowEditControl ChatMessage;
var UWindowSmallButton ChatButton;

var RuneMenuMapListExclude MapListBox;
var UWindowWrappedTextArea VoteList;
var RuneMenuLabelControl StatusLabel;
var WV_StatusListBox ListVoteStatus;


function Created() {
	local int HeaderFont;
	local RuneMenuMapList L;
	local WV_Rep VoteRep;
	local WV_WRI WindowRep;
	local string FirstMap, NextMap, TestMap;
	local int i, MapsLen;
	Super.Created();

	//Fix for smaller screens
	if (Root.GUIScale < 2) {
		HeaderFont = F_Large;
	}
	else {
		HeaderFont = F_RuneLarge;
	}

	// Game Type Label
	GameTypeLabel = RuneMenuLabelControl(CreateWindow(class'RuneMenuLabelControl', 20, 20, 140, 1));
	GameTypeLabel.SetText("Game Type");
	GameTypeLabel.SetFont(HeaderFont);
	GameTypeLabel.Textcolor.r = 250;
	GameTypeLabel.Textcolor.g = 250;
	GameTypeLabel.Textcolor.b = 250;
	GameTypeLabel.Align = TA_Left;
	GameTypeLabel.bDropShadow = true;
	GameTypeLabel.DropAmountX = 1;
	GameTypeLabel.DropAmountY = 1;

	// Game Type Dropdown
	GameTypeSelect = UWindowComboControl(CreateControl(class'UWindowComboControl', -120, 46, 280, 1));
	// Game list will be loaded on initial tick
	DrawGameList();

	// Map Lists Label
	MapListLabel = RuneMenuLabelControl(CreateWindow(class'RuneMenuLabelControl', 20, 74, 120, 1));
	MapListLabel.SetText("Map List");
	MapListLabel.SetFont(HeaderFont);
	MapListLabel.Textcolor.r = 250;
	MapListLabel.Textcolor.g = 250;
	MapListLabel.Textcolor.b = 250;
	MapListLabel.Align = TA_Left;
	MapListLabel.bDropShadow = true;
	MapListLabel.DropAmountX = 1;
	MapListLabel.DropAmountY = 1;

	// Map List Select
	MapListBox = RuneMenuMapListExclude(CreateWindow(class'RuneMenuMapListExclude', 20, 100, 140, 130, Self));
	MapListBox.Register(Self);
	DrawMapList();
	// Map list will be loaded on initial tick


	// Vote Button
	VoteButton = UWindowSmallButton(CreateControl(class'UWindowSmallButton', 20, 242, 140, 20));
	VoteButton.SetText("Select Map");
	VoteButton.NotifyWindow = Self;

	// Map Votes Label
	StatusLabel = RuneMenuLabelControl(CreateWindow(class'RuneMenuLabelControl', 180, 20, 180, 1));
	StatusLabel.SetText("Map Votes");
	StatusLabel.SetFont(HeaderFont);
  StatusLabel.Textcolor.r = 250;
  StatusLabel.Textcolor.g = 250;
  StatusLabel.Textcolor.b = 250;
	StatusLabel.Align = TA_Left;
	StatusLabel.bDropShadow = true;
	StatusLabel.DropAmountX = 1;
	StatusLabel.DropAmountY = 1;

	// Map Votes Status
	ListVoteStatus=WV_StatusListBox(CreateControl(Class'WV_StatusListBox', 180, 46, 196, 110));
	ListVoteStatus.bAcceptsFocus=False;
	ListVoteStatus.Items.Clear();

	// Vote Time Left
	VoteTimeLabel = RuneMenuLabelControl(CreateWindow(class'RuneMenuLabelControl', 350, 30, 20, 1));
	VoteTimeLabel.SetText("");
	VoteTimeLabel.SetFont(F_Normal);
	VoteTimeLabel.Textcolor.r = 250;
	VoteTimeLabel.Textcolor.g = 250;
	VoteTimeLabel.Textcolor.b = 250;
	VoteTimeLabel.Align = TA_Right;
	VoteTimeLabel.bDropShadow = true;
	VoteTimeLabel.DropAmountX = 1;
	VoteTimeLabel.DropAmountY = 1;

	// MOTD Text 1
	MOTD1Label = RuneMenuLabelControl(CreateWindow(class'RuneMenuLabelControl', 180, 170, 180, 1));
	MOTD1Label.SetText("Visit us at TheWarClan.com");
	MOTD1Label.SetFont(F_Normal);
	MOTD1Label.Textcolor.r = 250;
	MOTD1Label.Textcolor.g = 250;
	MOTD1Label.Textcolor.b = 250;
	MOTD1Label.Align = TA_Center;
	MOTD1Label.bDropShadow = true;
	MOTD1Label.DropAmountX = 1;
	MOTD1Label.DropAmountY = 1;

	// Website Button
	WebsiteButton = UWindowSmallButton(CreateControl(class'UWindowSmallButton', 180, 190, 196, 20));
	WebsiteButton.SetText("TheWarClan.com");
	WebsiteButton.NotifyWindow = Self;

	// Discord Button
	DiscordButton = UWindowSmallButton(CreateControl(class'UWindowSmallButton', 180, 210, 196, 20));
	DiscordButton.SetText("Rune Discord");
	DiscordButton.NotifyWindow = Self;

	// Chat Label
	ChatLabel = RuneMenuLabelControl(CreateWindow(class'RuneMenuLabelControl', 180, 244, 20, 1));
	ChatLabel.SetText("Chat");
	ChatLabel.SetFont(F_Normal);
	ChatLabel.Textcolor.r = 250;
	ChatLabel.Textcolor.g = 250;
	ChatLabel.Textcolor.b = 250;
	ChatLabel.Align = TA_Left;
	ChatLabel.bDropShadow = true;
	ChatLabel.DropAmountX = 1;
	ChatLabel.DropAmountY = 1;

	// Chat Message
	ChatMessage=UWindowEditControl(CreateControl(Class'UWindowEditControl',202,242,140,10));
	ChatMessage.EditBoxWidth = 140;
	ChatMessage.SetNumericOnly(False);
	ChatMessage.SetHistory(True);
	ChatMessage.SetMaxLength(80);
	ChatMessage.SetValue("");
	ChatMessage.Align=TA_Left;

	// Chat Send Button
	ChatButton = UWindowSmallButton(CreateControl(class'UWindowSmallButton', 346, 242, 30, 20));
	ChatButton.SetText("Send");
	ChatButton.NotifyWindow = Self;
}

function Tick(float deltatime) {
		local WV_Rep VoteRep;
		local WV_WRI WindowRep;
		local int i;
		local string TimeLeftString;
		local WV_StatusListItem S;

		if (VoteRep == none) {
			foreach Root.GetPlayerOwner().AllActors( class'WV_Rep', VoteRep )
			break;
		}
		if (WindowRep == none ) {
			 foreach Root.GetPlayerOwner().AllActors( class'WV_WRI', WindowRep )
			 break;
		}

		ListVoteStatus.Items.Clear();

		// lol bad code for formating
		if (VoteRep.VoteTimeLeft != -1) {
			// In seconds
			if(VoteRep.VoteTimeLeft > 60) {
				TimeLeftString = string(VoteRep.VoteTimeLeft / 60);
				if (VoteRep.VoteTimeLeft % 60 < 10) { //single digit padding
					TimeLeftString = TimeLeftString$":0"$string(VoteRep.VoteTimeLeft % 60);
				}
				else {
					TimeLeftString = TimeLeftString$":"$string(VoteRep.VoteTimeLeft % 60);
				}
			}
			else {
				if (VoteRep.VoteTimeLeft < 10) { //single digit padding
					TimeLeftString = "0:0"$VoteRep.VoteTimeLeft;
				}
				else {
					TimeLeftString = "0:"$VoteRep.VoteTimeLeft;
				}
			}
			VoteTimeLabel.SetText(TimeLeftString);
		}

		for (i=0; i<32; i++) {
			if (VoteRep.getVoteListTally(i) > 0) {
				S = WV_StatusListItem(ListVoteStatus.Items.Append(class'WV_StatusListItem'));
				S.MapName = VoteRep.getVoteListMap(i);
				S.rank = i+1;
				S.VoteCount = VoteRep.getVoteListTally(i);
			}
		}
		if (ListVoteStatus.Items.Count() <= 0) {
			S = WV_StatusListItem(ListVoteStatus.Items.Append(class'WV_StatusListItem'));
			S.MapName = VoteRep.NextMap;
			S.rank = 0;
			S.VoteCount = 0;
		}
	Super.Tick(deltatime);
}

function DrawMapList() {
		local WV_Rep VoteRep;
		local WV_WRI WindowRep;
		local string FirstMap, NextMap, TestMap;
		local int CurrentGame, i, MapsLen;
		local RuneMenuMapList L;

		MapListBox.Items.Clear();
		CurrentGame = GameTypeSelect.GetSelectedIndex();

		if (WindowRep == none ) {
			 foreach Root.GetPlayerOwner().AllActors( class'WV_WRI', WindowRep )
			 break;
		}
		if (VoteRep == none) {
			foreach Root.GetPlayerOwner().AllActors( class'WV_Rep', VoteRep )
			break;
		}

		i = 0;
		// MapsLen = WindowRep.LoadedMapNum;
		FirstMap = WindowRep.GetMap(CurrentGame,i);
		NextMap = FirstMap;
		if (FirstMap ~= "") {
			return;
		}
		while (!(FirstMap ~= TestMap) && !(NextMap ~= "")) {
			L = RuneMenuMapList(MapListBox.Items.Append(class'RuneMenuMapList'));
			L.MapName = string(i);
			if(Right(NextMap, 4) ~= ".run")
				L.DisplayName = Left(NextMap, Len(NextMap) - 4);
			else
				L.DisplayName = NextMap;
			i++;
			NextMap = WindowRep.GetMap(CurrentGame,i);
			TestMap = NextMap;
		}
}
function DrawGameList() {
	local WV_Rep VoteRep;
	local WV_WRI WindowRep;
	local string FirstMap, NextMap, TestMap;
	local string FirstGameType, NextGameType, TestGameType;
	local string MapList[64];
	local RuneMenuMapList L;
	local byte i;

	if (VoteRep == none ) {
		 foreach Root.GetPlayerOwner().AllActors( class'WV_Rep', VoteRep )
		 break;
	}
	if (WindowRep == none ) {
		 foreach Root.GetPlayerOwner().AllActors( class'WV_WRI', WindowRep )
		 break;
	}

	GameTypeSelect.Clear();

	i = 0;
	FirstGameType = WindowRep.GameTypes[0];
	NextGameType = FirstGameType;
	if (FirstGameType ~= "") {
		return;
	}
	while (!(FirstGameType ~= TestGameType) && !(NextGameType ~= "") && (i < 20)) {
		GameTypeSelect.AddItem(NextGameType);
		i++;
		NextGameType = WindowRep.GameTypes[i];
		TestGameType = NextGameType;
	}
	GameTypeSelect.SetSelectedIndex(VoteRep.CurrentGameNum);
}
function Paint(Canvas C, float MouseX, float MouseY) {
	local float X, Y, W, H;
	// Use this to put a logo or texture in the back of the menu
	C.DrawColor.R = 90;
	C.DrawColor.G = 90;
	C.DrawColor.B = 90;
	Super.Paint(C,X,Y);
	// Map Lists Background
	C.DrawColor.R = 255;
	C.DrawColor.G = 255;
	C.DrawColor.B = 255;
	DrawStretchedTexture(C, 20, 100, 140, 130, Texture'WhiteTexture');
	// Map Votes Background
	C.DrawColor.R = 90;
	C.DrawColor.G = 90;
	C.DrawColor.B = 90;
	DrawStretchedTexture(C, 180, 46, 196, 110, Texture'WhiteTexture');
}
function Notify(UWindowDialogControl C, byte E) {
		//Clicks
		if(E == DE_Click) {
			 switch(C) {
				case ChatButton:
					if(ChatMessage.GetValue() != "") {
						GetPlayerOwner().ConsoleCommand("SAY " $ ChatMessage.GetValue());
						ChatMessage.SetValue("");
					}
					break;
				case VoteButton:
					VotePressed();
					break;
				case WebsiteButton:
					GetPlayerOwner().ConsoleCommand("start http://thewarclan.com");
					break;
				case DiscordButton:
					GetPlayerOwner().ConsoleCommand("start https://discord.gg/7mjP862");
					break;
	  	}
		}
		// Events
		switch(E) {
		 	case DE_Change:
		 		switch(C) {
					case GameTypeSelect:
						GameChanged();
						break;
		 		}
		 		break;
		 	case DE_EnterPressed:
				if(ChatMessage.GetValue() != "") {
					GetPlayerOwner().ConsoleCommand("SAY " $ ChatMessage.GetValue());
					ChatMessage.SetValue("");
					ChatMessage.FocusOtherWindow(ChatButton);
				}
				break;
		 default:
	 }
}
function NotifyBeforeLevelChange() {
	Super.NotifyBeforeLevelChange();
	Close();
}
function Close(optional bool bByParent) {
	// makes sure the all the windows close at once
	Root.Console.bQuickKeyEnable = False;
	Root.Console.CloseUWindow();
	Super.Close(bByParent);
}
function WindowEvent(WinMessage Msg, Canvas C, float X, float Y, int Key) {
	Super.WindowEvent( Msg, C, X, Y, Key );
	if (Msg == WM_KeyDown && Key == 121) {
		Close();
	}
}
function EscClose() {
	Close();
}
function VotePressed() {
	 local WV_Rep VoteRep;
	 local int CurrentGame;
	 local string CurrentMap;

	 if ( VoteRep == none )
	 {
		 foreach Root.GetPlayerOwner().AllActors( class'WV_Rep', VoteRep )
		 break;
	 }
	 CurrentGame = GameTypeSelect.GetSelectedIndex();
	 if (MapListBox.SelectedItem == None) {
		 CurrentMap = "0";
	 }
	 else {
		 CurrentMap = RuneMenuMapList(MapListBox.SelectedItem).MapName;
		 GetPlayerOwner().ConsoleCommand("mutate mapvote "$RuneMenuMapList(MapListBox.SelectedItem).DisplayName);
	 }
}
function GameChanged() {
	local WV_Rep VoteRep;
	local WV_WRI WindowRep;
	local string FirstMap, NextMap, TestMap;
	local int CurrentGame, i;
	local RuneMenuMapList L;

	if (VoteRep == none ) {
		 foreach Root.GetPlayerOwner().AllActors( class'WV_Rep', VoteRep )
		 break;
	}
	if (WindowRep == none ) {
		 foreach Root.GetPlayerOwner().AllActors( class'WV_WRI', WindowRep )
		 break;
	}
	CurrentGame = GameTypeSelect.GetSelectedIndex();
	if (CurrentGame == -1) {
		return;
	}
	// GetPlayerOwner().ConsoleCommand("mutate mapvote list "$CurrentGame);
	DrawMapList();
}
function HideAllWindows() {
	ParentWindow.Close();
}

defaultproperties{}
