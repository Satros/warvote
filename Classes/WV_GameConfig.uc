//============================================================================
// Config Superclass
//============================================================================
class WV_GameConfig extends Info;
var() config string GameName;
var() config string GameClass;
var() config string GamePrefix;
var() config string GameMutators;
var() config string GameSettings;
var() config string Maps[64];
defaultproperties {}
