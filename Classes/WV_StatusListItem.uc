//================================================================================
// StatusListItem
//================================================================================
class WV_StatusListItem extends UWindowListBoxItem;

var int rank;
var string MapName;
var int VoteCount;

function int Compare (UWindowList t, UWindowList B) {
	if (Caps(WV_StatusListItem(t).MapName) < Caps(WV_StatusListItem(B).MapName)) {
		return -1;
	}
	return 1;
}
