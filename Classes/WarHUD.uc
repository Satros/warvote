class WarHUD expands Mutator;

var float FadeOutTime;
var bool HUTInitialized;
var int ShowMessageTime;
var color GlowColor;
var bool setKeyBinding;

simulated function PreBeginPlay() {
    FadeOutTime = 1000;
}
simulated function Tick(float DeltaTime) {
	Super.Tick(DeltaTime);
	if (!HUTInitialized) {
		ShowMessageTime +=1;
	}
	if (ShowMessageTime >= 10) {
    ShowMessageTime =0;
    RegisterHUDMutator();
  }
	FadeOutTime = FMax(0.0, FadeOutTime - DeltaTime * 55);
}
simulated function RegisterHUDMutator() {
	local HUD MyHud;
	local Pawn P;
	local playerpawn playerpawn;

  if (!HUTInitialized) {
		foreach AllActors(class'HUD', MyHud) {
			HUTInitialized = true;
      if (MyHud.owner != none) {
				if (MyHud.owner == owner) {
					NextHUDMutator = MyHud.HUDMutator;
					MyHud.HUDMutator = Self;
					bHUDMutator = True;
        }
				else {
					HUTInitialized = true;
					return;
				}
			}
			else {
				HUTInitialized = true;
				return;
			}
		}
	}
}
simulated event PostRender(canvas Canvas) {
	if (FadeOutTime > 0.0) {
		DrawHelpMessage(Canvas);
  }
	if (nextHUDMutator != None) {
		nextHUDMutator.PostRender(Canvas);
	}
}
simulated function DrawHelpMessage (canvas Canvas) {
	local float XXL,YYL;
	if (!setKeyBinding && Canvas.Viewport.Actor.IsA('PlayerPawn'))  {
		Canvas.Viewport.Actor.ConsoleCommand("set input f10 mutate mapvote menu");
		setKeyBinding = true;
	}
	Canvas.Font = Font'RuneCred';
	Canvas.Style=ERenderStyle.STY_Normal;
  Canvas.bCenter = true;
	Canvas.StrLen("Press [F10] to change the map or gametype", XXL, YYL);
	Canvas.SetPos(0,Canvas.ClipY - 150);
	GlowColor.R -= 2;
	GlowColor.G += 1;
	GlowColor.B += 3;
	Canvas.DrawColor = GlowColor;
	Canvas.DrawText("Press [F10] to change the map or gametype", true);
}

defaultproperties {
     bAlwaysRelevant=True
     RemoteRole=ROLE_SimulatedProxy
}
