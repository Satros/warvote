//================================================================================
// StatusListBox
//================================================================================
class WV_StatusListBox extends UWindowListBox;

function Created () {
	Super.Created();
	VertSB.Close();
	VertSB=UWindowVScrollbar(CreateWindow(Class'UWindowVScrollbar',WinWidth - 12,0.00,12.00,WinHeight));
}

function Paint (Canvas C, float MouseX, float MouseY) {
	C.DrawColor.R=255;
	C.DrawColor.G=255;
	C.DrawColor.B=255;
	// top outer line
	DrawStretchedTexture(C,0.00,0.00,WinWidth,1.00,Texture'WhiteTexture');
	// left outer line
	DrawStretchedTexture(C,0.00,0.00,1.00,WinHeight - 1,Texture'WhiteTexture');
	// MapName divider line
	DrawStretchedTexture(C,145.00,0.00,1.00,WinHeight - 1,Texture'WhiteTexture');
	// bottom line
	DrawStretchedTexture(C,0.00,WinHeight - 1,WinWidth,1.00,Texture'WhiteTexture');
	Super.Paint(C,MouseX,MouseY);
}

function DrawItem (Canvas C, UWindowList Item, float X, float Y, float W, float H) {
	local string sTemp;
	local int Count;

	if ( WV_StatusListItem(Item).bSelected ) {
		//draw blue background
		C.DrawColor.R=0;
		C.DrawColor.G=0;
		C.DrawColor.B=128;
	}
	else {
		C.DrawColor.R=255;
		C.DrawColor.G=255;
		C.DrawColor.B=255;
	}
	C.Font=Root.Fonts[0];
	sTemp = WV_StatusListItem(Item).MapName;
	if (Len(sTemp) > 25)
	    sTemp = Left(sTemp, 25);
	ClipText(C, X+5,  Y+2, sTemp);
	Count = WV_StatusListItem(Item).VoteCount;
	if (Count == 0) {
		ClipText(C, X + 150, Y+2, "Auto");
	}
	else {
		ClipText(C, X + 150, Y+2, string(Count));
	}
}

function SelectMap(string MapName) {
   local string sTemp;
   local WV_StatusListItem MapItem;

   for (MapItem = WV_StatusListItem(Items); MapItem != None; MapItem = WV_StatusListItem(MapItem.Next) )
   {
      sTemp = MapItem.MapName;
      if (Left(sTemp, 3) == "[G]")
         sTemp = Mid(sTemp, 3);
      if (MapName ~= sTemp)
      {
         SetSelectedItem(MapItem);
         MakeSelectedVisible();
         break;
      }
   }
}

function DoubleClickItem (UWindowListBoxItem i) {
	UWindowDialogClientWindow(ParentWindow).Notify(self, 11);
}

defaultproperties {
    ItemHeight=13.00
    ListClass=Class'WV_StatusListItem'
}
