//=============================================================================
// WarVoteFramedWindow.
//=============================================================================
class WV_FramedWindow expands UWindowFramedWindow;

function Created () {
	Super.Created ();
	bSizable = false;
	bAlwaysOnTop = True;
	bLeaveOnScreen = True;
	SetAcceptsFocus();
}
function SetDimensions () {
	SetSize(ParentWindow.WinWidth * (2/3), ParentWindow.WinWidth * (2/3));
	WinLeft = ParentWindow.WinWidth/2 - WinWidth/2;
	WinTop = ParentWindow.WinHeight/2 - WinHeight/2;
}
function ResolutionChanged (float W, float H) {
	SetDimensions ();
}

defaultproperties {
     ClientClass=Class'WarVote.WV_Display'
     WindowTitle="Rune Map Vote by Satros - TheWarClan.com"
		 bAcceptsHotKeys=True
}
