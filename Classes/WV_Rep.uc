//=============================================================================
// WarVote Replication
//=============================================================================
class WV_Rep extends ReplicationInfo;

// Game Types, Maps, Maplist
var string NextMap;
var int CurrentGameNum;

// Votes
struct VotesListItem {
	var int GameNum;
	var int MapNum;
	var int VotesNum;
};

var VotesListItem VoteList[32];
var string VoteListMaps[32];

// Time left to vote
var int VoteTimeLeft;

replication {
	reliable if ( Role == ROLE_Authority ) // Functions
		getVoteListMap, getVoteListTally, addVoteToList, removeVoteFromList;
	reliable if ( Role == ROLE_Authority ) // Configs
	  NextMap, CurrentGameNum, VoteTimeLeft;
	reliable if ( Role == ROLE_Authority ) // Votes
		VoteList, VoteListMaps;
}

//============================================================
// Add Vote to List - Adds a map vote to list of map votes
//============================================================
simulated function addVoteToList(int GameNum, int MapNum, string MapName) {
	local int i, j;
	local bool bFound;
	local int tempGameNum;
	local int tempMapNum;
	local string tempMapName;
	local int tempVotesNum;

	log("WarVote - Function addVoteToList");
	// Do sorting
	for (i=0; i<32; i++) {
		// Found in list
		// if gametype and map num are the same
		if (VoteList[i].GameNum == GameNum && VoteList[i].MapNum == MapNum) {
			// increase number of votes
			log("WarVote - increment vote");
			VoteList[i].VotesNum++;
			//Fix issue where first map of first gametype would not display
			VoteListMaps[i] = MapName;
			// go up the list, keep swapping upwards while higher vote num
			for (j=i; j > 0 && VoteList[j].VotesNum > VoteList[j-1].VotesNum; j--) {

				log("WarVote - swap "$VoteListMaps[j-1]$" and "$VoteListMaps[j]);
				// save above to temp
			 	tempGameNum = VoteList[j-1].GameNum;
				tempMapNum = VoteList[j-1].MapNum;
				tempVotesNum = VoteList[j-1].VotesNum;
				tempMapName = VoteListMaps[j-1];

				// set one above to current
				VoteList[j-1].GameNum = VoteList[j].GameNum;
				VoteList[j-1].MapNum = VoteList[j].MapNum;
				VoteList[j-1].VotesNum = 	VoteList[j].VotesNum;
				VoteListMaps[j-1] = VoteListMaps[j];

				//  set current to one above
				VoteList[j].GameNum = tempGameNum;
				VoteList[j].MapNum = tempMapNum;
				VoteList[j].VotesNum = tempVotesNum;
				VoteListMaps[j] = tempMapName;

			}
			return;
		}
		// Add to end of list
		else if (VoteList[i].VotesNum == 0) {
			log("WarVote - add new vote "$i$" "$MapName);
			VoteList[i].GameNum = GameNum;
			VoteList[i].MapNum = MapNum;
			VoteList[i].VotesNum = 1;
			VoteListMaps[i] = MapName;
			return;
		}
	}
}
//============================================================
// Remove Vote from List - Removes a map vote from list of map votes
//============================================================
simulated function removeVoteFromList(int GameNum, int MapNum) {
	local int i, j;
	local bool bFound;
	local int tempGameNum;
	local int tempMapNum;
	local string tempMapName;
	local int tempVotesNum;

	log("WarVote - Function removeVoteFromList");
	//Do sorting
	for (i=0; i<32; i++) {
		// find vote to remove in list
		if (VoteList[i].GameNum == GameNum && VoteList[i].MapNum == MapNum) {
			log("WarVote - decrement vote");
			VoteList[i].VotesNum -= 1;
			// while votesnum is less than one below
			for (j=i; j < 32 && VoteList[j].VotesNum < VoteList[j+1].VotesNum && VoteList[j+1].VotesNum > 0; j++) {

				log("WarVote - swap "$VoteListMaps[j+1]$" and "$VoteListMaps[j]);
				tempGameNum = VoteList[j+1].GameNum;
				tempMapNum = VoteList[j+1].MapNum;
				tempVotesNum = VoteList[j+1].VotesNum;
				tempMapName = VoteListMaps[j+1];


				VoteList[j+1].GameNum = VoteList[j].GameNum;
				VoteList[j+1].MapNum = VoteList[j].MapNum;
				VoteList[j+1].VotesNum = 	VoteList[j].VotesNum;
				VoteListMaps[j+1] = VoteListMaps[j];

				VoteList[j].GameNum = tempGameNum;
				VoteList[j].MapNum = tempMapNum;
				VoteList[j].VotesNum = tempVotesNum;
				VoteListMaps[j] = tempMapName;

			}
			return;
		}
	}
}

simulated function string getVoteListMap(int index) {
	return VoteListMaps[index];
}
simulated function int getVoteListTally(int index) {
	return VoteList[index].VotesNum;
}
simulated function int getTopVoteMap() {
	return VoteList[0].MapNum;
}
simulated function int getTopVoteGame() {
	return VoteList[0].GameNum;
}


defaultproperties {
    RemoteRole=ROLE_SimulatedProxy
}
