//=============================================================================
// WarVoteMutator
//=============================================================================
class WarVoteMutator extends Mutator;

var WV_GameTypes GameTypes;

var bool bWindowOpen;
var WV_Rep VoteRep;
Var windowconsole TVConsole;

// Player Info
var int CurrentPlayerCount;
var int PlayerIDList[32];
struct PlayerVotes { var int GameNum, MapNum; };
var PlayerVotes PlayerVote[32];
var bool bPlayerLoading;

var bool bGameHasEnded;
var bool bLevelSwitchPending;

var string ServerTravelString;

// DebugLog
var() config bool bDebugLog;

// Default map rotation, can have different gametypes
var() config string Maps[32];
var() config int NextMapNum;
var() config int CurrentGameNum;

//Times
var() config int EmptySwitchTime;
var() config int StartVoteDelay;
var() config int EndScoreBoardDelay;
var() config int EndVoteTimeLimit;
var int EndTimeLeft;
var int EmptyTimeLeft;

// Data Buffer Control
var() config int MaxDataSend;

//============================================================
// PreBeginPlay - Loads configs and replicates them
//============================================================
function PreBeginPlay () {
	if (bDebugLog) {
		log("WarVote - Function PreBeginPlay");
	}
  // Load game type configs
	if (GameTypes == None && !bLevelSwitchPending) {
			log("WarVote - Spawning Game Type Manager");
			GameTypes = Spawn(class'WV_GameTypes');
			log("WarVote - Loading Game Types");
			GameTypes.LoadGameTypes();
	}
  // Replicate game types and maps
	log("WarVote - Loading Vote Replication");
	LoadVoteReplication();
  // Let the mutator take control of end game changes
	RuneMultiPlayer(Level.Game).bDontRestart = True;
	log("WarVote - Loaded Configs");
	EmptyTimeLeft = EmptySwitchTime;
	log("WarVote - EmptyTimeLeft="$EmptyTimeLeft);
	Super.PreBeginPlay();
}
//============================================================
// LoadVoteReplication - Creates replication and replicates maps and gametypes
//============================================================
function LoadVoteReplication() {
	local byte i;
	local byte j;
	local byte mapIndex;
	local string gameName;
	local string mapName;

	if (bDebugLog) {
		log("WarVote - Function LoadVoteReplication");
	}
  // Set all player votes to -1 (no vote)
	log("WarVote - Setting players");
	for (i=0; i<32; i++) {
		PlayerIDList[i] = -1;
		PlayerVote[i].GameNum=-1;
		PlayerVote[i].MapNum=-1;
	}
  // Spawn vote replication
	if (VoteRep == None) {
		log("WarVote - Spawning rep");
		VoteRep = Spawn(class'WV_Rep');
	}
  // Set endgame vote timer to -1
	VoteRep.VoteTimeLeft = -1;
	VoteRep.CurrentGameNum = CurrentGameNum;

  // Set default next map from maplist
	VoteRep.NextMap = Maps[NextMapNum];
}
//============================================================
// ModifyPlayer - Spawns help message HUD
//============================================================
function ModifyPlayer(Pawn Other) {
    local WarHUD myHUD;
		local WV_WRI VoteWRI;
    Super.ModifyPlayer(Other);
		if (bDebugLog) { log("WarVote - Function ModifyPlayer"); }
    foreach AllActors(class'WarHUD', myHUD) {
			 if (myHUD.owner == Other) {
             return;
			 }
    }
    Spawn(Class'WarVote.WarHUD',other);

		// Might have to change for mods that don't respect the chain
		VoteWRI = GetOrCreateWRI(PlayerPawn(Other));
}
//============================================================
// Mutate - Mutator commands
//============================================================
function Mutate(string MutateString, PlayerPawn Sender) {
	local string arg, gameNum;
	local int i, j;
	local WV_WRI VoteWRI;

	if (bDebugLog) {
		log("WarVote - Function Mutate :"$MutateString);
	}
  // Starts with MUTATE MAPVOTE
	if (left(Caps(MutateString),8) == "MAPVOTE ") {
		arg = Mid(MutateString, 8);
    // MUTATE MAPVOTE MENU
		if (Caps(arg) == "MENU") {
			OpenVoteWindow(Sender);
		}
		else { // MUTATE MAPVOTE <MAPNAME>
				SubmitVote(arg, Sender);
		}
		return;
	}
	Super.Mutate(MutateString,Sender);
}
function WV_WRI GetOrCreateWRI(PlayerPawn Voter) {
	local WV_WRI VoteWRI;
	local int i, j;
	// if (bDebugLog) { log("WarVote - Function Get Or Create WRI"); }
  // Dont create if already exists
	foreach AllActors(Class'WV_WRI',VoteWRI) {
		if (Voter == VoteWRI.Owner) {
			return VoteWRI;
		}
	}
  // Create Window and Open
	VoteWRI = Spawn(Class'WV_WRI',Voter,,Voter.Location);
	// If newly created, start loading data into it
	VoteWRI.bPlayerLoading = true;
	bPlayerLoading = true;
	return VoteWRI;
}
//============================================================
// OpenVoteWindow - Opens player's Vote Window
//============================================================
function OpenVoteWindow(PlayerPawn Voter) {
	local WV_WRI VoteWRI;
	local int i, j;
	local WV_Rep VoteRepClient;

	if (bDebugLog) { log("WarVote - Function OpenVoteWindow"); }

	if (bLevelSwitchPending) {
		return;
	}

	VoteWRI = GetOrCreateWRI(Voter);
	if (VoteWRI.bPlayerLoading) {
		Voter.ClientMessage("Please wait while the maps are being loaded.");
		return;
	}

	VoteWRI.OpenWindow();
	if (VoteWRI == None ) {
		Log("#### -- PostLogin :: Fail:: Could not spawn WRI");
		return;
	}
}
//============================================================
// CloseVoteWindow - Closes player's Vote Window
//============================================================
function CloseVoteWindow(PlayerPawn Voter) {
	local WV_WRI VoteWRI;
	if (bDebugLog) { log("WarVote - Function CloseVoteWindow"); }
  // Find Player's Vote Window and close it
	foreach AllActors (class 'WV_WRI', VoteWRI) {
  	if (Voter == VoteWRI.Owner) {
  		VoteWRI.CloseWindow();
  		VoteWRI.Destroy();
  		return;
  	}
  	else {
  		VoteWRI = None;
    }
  }
}
//============================================================
// CloseAllVoteWindows - Closes all players Vote Windows
//============================================================
function CloseAllVoteWindows() {
	local WV_WRI VoteWRI;
	if (bDebugLog) { log("WarVote - Function CloseAllVoteWindows"); }
	foreach AllActors(Class'WV_WRI',VoteWRI)
	{
		VoteWRI.CloseWindow();
		VoteWRI.Destroy();
	}
}
//============================================================
// Tick - tracks player join, player leave, game end
//============================================================
function Tick(float DeltaTime) {
	Super.Tick(DeltaTime);
	// Player Join Event
	if (Level.Game.NumPlayers > CurrentPlayerCount) {
		if (bDebugLog) {
			log("WarVote - Player Join Tick");
		}
		CurrentPlayerCount = Level.Game.NumPlayers;
		onPlayerJoin();
	}
	// Player Left Event
	if (Level.Game.NumPlayers < CurrentPlayerCount) {
		if (bDebugLog) {
			log("WarVote - Player Left Tick");
		}
		CurrentPlayerCount = Level.Game.NumPlayers;
		onPlayerLeave();
	}
  // Game End Event
	if (Level.Game.bGameEnded && !bGameHasEnded) {
		if (bDebugLog) {
			log("WarVote - End Game Tick");
		}
	  onGameEnd();
	}
	// Sending Player Data
	if(bPlayerLoading == true) {
		LoadDataBatch();
	}
}
//============================================================
// onPlayerJoin - called when a player joins
//============================================================
function onPlayerJoin() {
  // Binds the F10 key to mapvote menu
	local Pawn aPawn;
	if (bDebugLog) {
		log("WarVote - Function onPlayerJoin");
	}
	for (aPawn=Level.PawnList; aPawn!=None; aPawn=aPawn.NextPawn ) {
			if (aPawn.bIsPlayer && PlayerPawn(aPawn) != none) {
					PlayerPawn(aPawn).ConsoleCommand("set input f10 mutate mapvote menu");
			}
	}
}
//============================================================
// onPlayerLeave - called when a player leaves
//============================================================
function onPlayerLeave() {
	if (bDebugLog) {
		log("WarVote - Function onPlayerLeave");
	}
	// Remove player's ID and vote
	CleanUpPlayerIDs();
	// Start server empty timer if all players have left
	if (CurrentPlayerCount == 0) {
		if (!bGameHasEnded) {
			SetTimer(60.00,false);
		}
		else {
			TravelNextMap();
		}
	}
	// Retally votes with the new player count
	TallyVotes();
}
//============================================================
// onGameEnd - called when game ends
//============================================================
function onGameEnd() {
	if (bDebugLog) {
		log("WarVote - Function onGameEnd");
	}
	// If there are no players, travel to next map from maplist
	bGameHasEnded = True;
	if (CurrentPlayerCount == 0) {
		TravelNextMap();
	}
  // If there are players, start end game voting timer
	else {
		EndTimeLeft = EndScoreBoardDelay + EndVoteTimeLimit;
		SetTimer(1.00,True);
	}
}
//============================================================
// Timer - Called at regular intervals to time end game voting
//         and force map change if server is empty too long
//============================================================
event Timer() {
	local Pawn aPawn;

	Super.Timer();
	// Server Empty Timer - tracks number of minutes the server has been empty
	if(!bGameHasEnded) { // Only relevant if game hasn't ended
    // If server is empty
		if (CurrentPlayerCount == 0) {
      // Decrement empty server time if max empty time hasn't been reached yet
			if (EmptyTimeLeft > 0) {
				EmptyTimeLeft--;
				log("WarVote - EmptyTimeLeft Decrement");
				SetTimer(60.00,false);
			}
      // Server has been empty for max time, end game and change map
			else {
				log("War Vote - Server has been empty for max time, change map.");
				TravelNextMap();
				// Level.Game.EndGame("");
				// Just travel to the next map instead
			}
		}
		else {  // Server has players, reset empty server timeout to default
			//Reset server empty timer
			log("WarVote - Reset EmptyTimeLeft");
			EmptyTimeLeft = EmptySwitchTime;
		}
	}

	// End Game Timer - runs every second after game end while players are in game
	if (bGameHasEnded && !bLevelSwitchPending) {
    // Decrement time left to vote
		if (EndTimeLeft > 0) {
			EndTimeLeft--;
			if (bDebugLog) {
				log("WarVote - End Time Left:" $ EndTimeLeft);
			}
      // Start end game voting
			if (EndTimeLeft == EndVoteTimeLimit) {
				// Open Voting Windows for all players for end game voting
				if (bDebugLog) {
					log("WarVote - End Game Voting");
				}
				for (aPawn=Level.PawnList; aPawn!=None; aPawn=aPawn.NextPawn) {
          if (aPawn.bIsPlayer && PlayerPawn(aPawn) != none) {
          		OpenVoteWindow(PlayerPawn(aPawn));
          }
				}
			}
      // Replicate final vote time remaining
			if (EndTimeLeft <= EndVoteTimeLimit) {
				VoteRep.VoteTimeLeft = EndTimeLeft;
			}
		}
    // If time is out, tally votes and travel to next map
		else {
			TallyVotes();
		}
	}
}
//============================================================
// Submit Vote - Called when a player votes for a map
//============================================================
function SubmitVote(string MapName, PlayerPawn Voter) {
	local int PlayerIndex;
	local int GameNum;
	local int MapNum;
	if (bDebugLog) {
		log("WarVote - Function SubmitVote");
	}
	// Map changing
	if(bLevelSwitchPending) {
		return;
	}
  // Find the gametype's index
	GameNum = GameTypes.findGameIndex(MapName);
	if(GameNum == -1) { // Gametype not found
		return;
	}
  // Find the map's index
	MapNum = GameTypes.findMapIndex(GameNum, MapName);
	if(MapNum == -1) { // Map not found
		return;
	}
  // Remove any players and their votes if they left
	CleanUpPlayerIDs();
  // Find this player's index, create it if necessary
	PlayerIndex = FindPlayerIndex(Voter.PlayerReplicationInfo.PlayerID,True);
  // If player isn't found return
	if (PlayerIndex == -1) {
		return;
	}
	// If this adminvote just force map change
	if ((Voter.PlayerReplicationInfo.bAdmin) || (Voter.bAdmin && Level.NetMode != NM_Standalone)) {
		// Remember gametype for next map
		CurrentGameNum = GameNum;
		SaveConfig();
		BroadcastMessage("Admin forced map change! Now traveling to " $ GameTypes.getMetaMapName(GameNum,MapNum));
		TravelToNewMap(GameTypes.getTravelString(GameNum, MapNum));
		return;
	}

	// Don't do anything if player's vote hasn't changed
	if (PlayerVote[PlayerIndex].GameNum == GameNum && PlayerVote[PlayerIndex].MapNum == MapNum) {
		Voter.ClientMessage("You have already voted for this map.");
		return;
	}

	// Remove player's previous vote if they have one
	if (PlayerVote[PlayerIndex].GameNum != -1) {
		VoteRep.removeVoteFromList(PlayerVote[PlayerIndex].GameNum, PlayerVote[PlayerIndex].MapNum);
	}
  // Set player's current map vote
	PlayerVote[PlayerIndex].GameNum = GameNum;
	PlayerVote[PlayerIndex].MapNum = MapNum;

	VoteRep.addVoteToList(PlayerVote[PlayerIndex].GameNum,PlayerVote[PlayerIndex].MapNum, GameTypes.getMetaMapName(GameNum, MapNum));

  if (bGameHasEnded) {
    BroadcastMessage(Voter.PlayerReplicationInfo.PlayerName $ " has voted for next map " $ MapName);
  }
  else {
    BroadcastMessage(Voter.PlayerReplicationInfo.PlayerName $ " has voted to change map to " $ MapName);
    // Tally the votes and see if the map should be changed
    TallyVotes();
  }
}
//============================================================
// Tally Vote - Check the votes to see if the map should be changed
//============================================================
function TallyVotes()
{
	local float VotePercent;
	local string travelString;

	if (bDebugLog) { log("WarVote - Function TallyVotes"); }

	if(bLevelSwitchPending) {
		return;
	}
  // Remove any players and their votes if they left
	CleanUpPlayerIDs();
	// If game hasn't ended - midgame vote
	if (!bGameHasEnded) {
    VotePercent = float(VoteRep.getVoteListTally(0)) / float(CurrentPlayerCount) * 100;
    // If majority have voted for top map
    if (VotePercent > 50.00) {
        travelString = GameTypes.getTravelString(VoteRep.getTopVoteGame(), VoteRep.getTopVoteMap());
				// Remember gametype for next map
				CurrentGameNum = VoteRep.getTopVoteGame();
				SaveConfig();
        BroadcastMessage("Map vote ended! Now traveling to " $ GameTypes.getMetaMapName(VoteRep.getTopVoteGame(),VoteRep.getTopVoteMap()));
				TravelToNewMap(travelString);
				return;
    }
  }
  else { // Game has ended
	 	if (EndTimeLeft <= 0) {
			// Travel to maplist default if no one voted
			if (VoteRep.getVoteListTally(0) < 1) {
				TravelNextMap();
			}
      // Travel to top voted map
			else {
				travelString = GameTypes.getTravelString(VoteRep.getTopVoteGame(), VoteRep.getTopVoteMap());
				// Remember gametype for next map
				CurrentGameNum = VoteRep.getTopVoteGame();
				SaveConfig();
				BroadcastMessage("Map vote ended! Now traveling to " $ GameTypes.getMetaMapName(VoteRep.getTopVoteGame(),VoteRep.getTopVoteMap()));
				TravelToNewMap(travelString);
				return;
			}
		}
	}
}
//============================================================
// TravelNextMap - Finds a player's index and adds them if they are not found
//============================================================
function TravelNextMap() {
	local int GameIndex;
	local int MapIndex;

	if (bDebugLog) { log("WarVote - Function TravelNextMap"); }

  // Check if there is a next map
	if (Maps[NextMapNum] != "") {
		GameIndex = GameTypes.findGameIndex(Maps[NextMapNum]);
		if (GameIndex > -1) { // next  game is found
			MapIndex = GameTypes.findMapIndex(GameIndex, Maps[NextMapNum]);
			if (MapIndex > -1) { // next  map is found
				// Increment and save next map number
				NextMapNum++;
				if (NextMapNum > 32 || Maps[NextMapNum] == "") { // Wrap
					NextMapNum = 0;
				}
				CurrentGameNum = GameIndex;
				SaveConfig();
				// Travel to next map
				log("WarVote - Traveling to next map");
        BroadcastMessage("Now traveling to " $ GameTypes.getMetaMapName(GameIndex, MapIndex));
				TravelToNewMap(GameTypes.getTravelString(GameIndex, MapIndex));
				return;
			}
		}
	}
	// Restart current map if there are any issues
	log("WarVote - Map not found, restarting same map");
	TravelToNewMap("?restart");
}
//============================================================
// TravelToNewMap - Travels to a new map
//============================================================
function TravelToNewMap(String TravelString) {
	bLevelSwitchPending = True;
	CloseAllVoteWindows();
	Level.ServerTravel(TravelString, False);
}
//============================================================
// FindPlayerIndex - Finds a player's index and adds them if they are not found
//============================================================
function int FindPlayerIndex (int PlayerID, bool bAddIfNotFound) {
	local int i;

	rsmDebugLog("Function - FindPlayerIndex");


	for (i=0; i<32; i++) {
    if (PlayerIDList[i] == PlayerID) {
			return i;
		}
	}
	if (bAddIfNotFound) {
		for (i=0; i<32; i++) {
      if (PlayerIDList[i] == -1) {
				PlayerIDList[i] = PlayerID;
				// Set vopte defaults
				PlayerVote[i].GameNum=-1;
				PlayerVote[i].MapNum=-1;
				return i;
			}
		}
	}
	return -1;
}
//============================================================
// CleanUpPlayerIDs - Removes votes and player id from any players who have left
//============================================================
function CleanUpPlayerIDs()
{
	local Pawn aPawn;
	local int x;
	local bool bFound;

	rsmDebugLog("Function - CleanUpPlayerIDs");


  for (x=0; x<32; x++) {
    if (PlayerIDList[x] >- 1) {
			bFound = false;
      for (aPawn=Level.PawnList; aPawn!=None; aPawn=aPawn.NextPawn) {
      	if (aPawn.bIsPlayer && aPawn.IsA('PlayerPawn') && PlayerPawn(aPawn).PlayerReplicationInfo.PlayerID == PlayerIDList[x]) {
        	bFound = true;
          break;
        }
      }
			if (!bFound) {
        // If has previous vote, remove it
      	if (PlayerVote[x].GameNum != -1) {
      		VoteRep.removeVoteFromList(PlayerVote[x].GameNum, PlayerVote[x].MapNum);
      	}
      	PlayerVote[x].GameNum=-1;
      	PlayerVote[x].MapNum=-1;
        // Remove player's ID
				PlayerIDList[x]=-1;
			}
    }
  }
}
//
//============================================================
// LoadDataBatch - Data send limiter, limits the amount of data sent as to not go over unreal's limits and crash.
//============================================================
function LoadDataBatch() {
	local int SendCount;
	MaxDataSend = 5;
	SendCount = 0;
	rsmDebugLog("Sending Batch Data to Player");

	while (bPlayerLoading && SendCount < MaxDataSend) {
		LoadDataItem();
		SendCount++;
	}
	return;
}
//============================================================
// LoadDataItem - Loads one data item (GameType or MapName) into a player's WRI
//============================================================
function LoadDataItem() {
	local WV_WRI VoteWRI;
	local string NextGame, NextMap;

	foreach AllActors(Class'WV_WRI',VoteWRI) {
		// Start loading new gametype
		if (VoteWRI.LoadedGameNum < 20) {
			NextGame = GameTypes.getGameName(VoteWRI.LoadedGameNum);
			if(VoteWRI.GameTypes[VoteWRI.LoadedGameNum] == "" && NextGame != "") {
				VoteWRI.GameTypes[VoteWRI.LoadedGameNum] = NextGame;
				// Log("Loading Game "$NextGame);
				// Set flag to start loading maps for this game type
				VoteWRI.LoadedMapNum = 0;
				return;
			}
		}

		// Start loading maps
		if (VoteWRI.LoadedGameNum < 20 && VoteWRI.LoadedMapNum < 100) {
			NextMap = GameTypes.getMetaMapName(VoteWRI.LoadedGameNum, VoteWRI.LoadedMapNum);
			if(NextMap != "") {
					VoteWRI.SetMap(VoteWRI.LoadedGameNum, VoteWRI.LoadedMapNum, NextMap);
					// Log("Loading Map "$NextMap);
					VoteWRI.LoadedMapNum++;
					return;
			} else {
				VoteWRI.LoadedGameNum++;
				return;
			}
		}
		VoteWRI.bPlayerLoading = false;
	}
	bPlayerLoading = false;
	return;
}
function rsmLog(String LogMessage) {
	if (bDebugLog) {
		log("{{RSM}} - " $ LogMessage);
	}
}
function rsmDebugLog(String LogMessage) {
	if (bDebugLog) {
		log("{{RSM}} - " $ LogMessage);
	}
}
// Default Properties
defaultproperties {
  StartVoteDelay=15
	EmptySwitchTime=20
	EndScoreBoardDelay=5
	EndVoteTimeLimit=30
	MaxDataSend=5
	bDebugLog=False
}
