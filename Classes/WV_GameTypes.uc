class WV_GameTypes extends Info;

var WV_GameConfig GameConfigs[32];

function string getGameName(int i) {
	return GameConfigs[i].GameName;
}
function string getGamePrefix(int i) {
	return GameConfigs[i].GamePrefix;
}
function string getTravelString(int GameIndex, int MapIndex) {
	local string mutators;
	mutators = "WarVote.WarVoteMutator";
	if (GameConfigs[GameIndex].GameMutators != "") {
		mutators = mutators$","$GameConfigs[GameIndex].GameMutators;
	}
	return getRealMapName(GameIndex, MapIndex)$"?game="$GameConfigs[GameIndex].GameClass$"?mutator="$mutators$GameConfigs[GameIndex].GameSettings;
}
function string getMetaMapName(int i, int j) {
	local int prefixEnd;
	local bool bCoopSearch;
	local string mapName;
	if (GameConfigs[i].Maps[j]!="") {
		mapName = GameConfigs[i].Maps[j];
		// Remove .run if found
		if(Right(mapName, 4) ~= ".run") {
			mapName = Left(mapName, Len(mapName) - 4);
		}
		// DM-Hildir -> TDM-Hildir etc
		prefixEnd = findPrefixEnd(mapName);
		if(prefixEnd != -1){ //If found prefix ending
			return GameConfigs[i].GamePrefix$Right(mapName, Len(mapName)-(prefixEnd));
		}
	}
	return "";
}
function string getRealMapName(int i, int j) {
	return GameConfigs[i].Maps[j]$".run";
}
function int findGameIndex(string MapName) {
	local byte i;
	local string mapPrefix;
	mapPrefix = Left(MapName, findPrefixEnd(MapName));
	//Do coop check
	// search gametypes
	for (i=0; i<20; i++) {
		if(Caps(mapPrefix) == Caps(getGamePrefix(i))) {
			return i;
		}
	}
	return -1;
}
// search through maps to find matching map, return map index
function int findMapIndex(int GameIndex, string MapName) {
	// search through game types to find matching prefix
	local byte i;
	for (i=0; i<32; i++) {
		if(Caps(MapName) == Caps(getMetaMapName(GameIndex, i))) {
			return i;
		}
	}
	return -1;
}
// search through game types to find matching prefix, return game index
function int findPrefixEnd(string MapName) {
	local int prefixEnd;
	local bool bCoopSearch;
	prefixEnd = InStr(mapName, "-");
	bCoopSearch = true;
	if(prefixEnd != -1){
		// Check if there are numbers to the left of the dash, if so include them
		while (bCoopSearch && prefixEnd > 0) {
			switch (Mid(mapName, prefixEnd-1, 1)) {
				case "0":
				case "1":
				case "2":
				case "3":
				case "4":
				case "5":
				case "6":
				case "7":
				case "8":
				case "9":
					prefixEnd = prefixEnd-1;
					break;
				default:
					bCoopSearch = false;
			}
		}
	}
	return prefixEnd;
}

function LoadGameTypes()
{
	// Country girls make do
	GameConfigs[0] = spawn(class'WVConfig_Game_1');
	GameConfigs[1] = spawn(class'WVConfig_Game_2');
	GameConfigs[2] = spawn(class'WVConfig_Game_3');
	GameConfigs[3] = spawn(class'WVConfig_Game_4');
	GameConfigs[4] = spawn(class'WVConfig_Game_5');
	GameConfigs[5] = spawn(class'WVConfig_Game_6');
	GameConfigs[6] = spawn(class'WVConfig_Game_7');
	GameConfigs[7] = spawn(class'WVConfig_Game_8');
	GameConfigs[8] = spawn(class'WVConfig_Game_9');
	GameConfigs[9] = spawn(class'WVConfig_Game_10');
	GameConfigs[10] = spawn(class'WVConfig_Game_11');
	GameConfigs[11] = spawn(class'WVConfig_Game_12');
	GameConfigs[12] = spawn(class'WVConfig_Game_13');
	GameConfigs[13] = spawn(class'WVConfig_Game_14');
	GameConfigs[14] = spawn(class'WVConfig_Game_15');
	GameConfigs[15] = spawn(class'WVConfig_Game_16');
	GameConfigs[16] = spawn(class'WVConfig_Game_17');
	GameConfigs[17] = spawn(class'WVConfig_Game_18');
	GameConfigs[18] = spawn(class'WVConfig_Game_19');
	GameConfigs[19] = spawn(class'WVConfig_Game_20');
	// GameConfigs[20] = spawn(class'WVConfig_Game_21');
	// GameConfigs[21] = spawn(class'WVConfig_Game_22');
	// GameConfigs[22] = spawn(class'WVConfig_Game_23');
	// GameConfigs[23] = spawn(class'WVConfig_Game_24');
	// GameConfigs[24] = spawn(class'WVConfig_Game_25');
	// GameConfigs[25] = spawn(class'WVConfig_Game_26');
	// GameConfigs[26] = spawn(class'WVConfig_Game_27');
	// GameConfigs[27] = spawn(class'WVConfig_Game_28');
	// GameConfigs[28] = spawn(class'WVConfig_Game_29');
	// GameConfigs[29] = spawn(class'WVConfig_Game_30');
	// GameConfigs[30] = spawn(class'WVConfig_Game_31');
	// GameConfigs[31] = spawn(class'WVConfig_Game_32');
}
